
HEADERS += \
    $$JSONIO17_HEADERS_DIR/jsonio17/type_test.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/exceptions.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/service.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/jsondetail.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/jsondump.h   \
    $$JSONIO17_HEADERS_DIR/jsonio17/jsonbase.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/jsonbuilder.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/jsonparser.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/jsonfree.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/txt2file.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/io_settings.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/schema.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/schema_thrift.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/jsonschema.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbquerybase.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbdriverbase.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbdriverarango.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbconnect.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbcollection.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbdocument.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbjsondoc.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbschemadoc.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbvertexdoc.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/dbedgedoc.h \
    $$JSONIO17_HEADERS_DIR/jsonio17/traversal.h


SOURCES += \
    $$JSONIO17_DIR/exceptions.cpp \
    $$JSONIO17_DIR/service.cpp \
    $$JSONIO17_DIR/jsondetail.cpp \
    $$JSONIO17_DIR/jsondump.cpp  \
    $$JSONIO17_DIR/jsonbase.cpp \
    $$JSONIO17_DIR/jsonbuilder.cpp \
    $$JSONIO17_DIR/jsonparser.cpp \
    $$JSONIO17_DIR/jsonfree.cpp \
    $$JSONIO17_DIR/txt2file.cpp \
    $$JSONIO17_DIR/io_settings.cpp \
    $$JSONIO17_DIR/schema_thrift.cpp \
    $$JSONIO17_DIR/jsonschema.cpp \
    $$JSONIO17_DIR/dbquerybase.cpp \
    $$JSONIO17_DIR/dbdriverarango.cpp \
    $$JSONIO17_DIR/dbconnect.cpp \
    $$JSONIO17_DIR/dbcollection.cpp \
    $$JSONIO17_DIR/dbdocument.cpp \
    $$JSONIO17_DIR/dbjsondoc.cpp \
    $$JSONIO17_DIR/dbschemadoc.cpp \
    $$JSONIO17_DIR/dbvertexdoc.cpp \
    $$JSONIO17_DIR/dbedgedoc.cpp \
    $$JSONIO17_DIR/traversal.cpp



