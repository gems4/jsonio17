find_package(jsonarango REQUIRED)
if(NOT jsonarango_FOUND)
    message(FATAL_ERROR "jsonarango library not found")
endif()

if(USE_SPDLOG_PRECOMPILED)
   if(NOT TARGET spdlog::spdlog)
       find_package(spdlog CONFIG REQUIRED)
       if(NOT spdlog_FOUND)
           message(FATAL_ERROR "spdlog not found")
       endif()
   endif()
endif()
